package ru.paulerokhin.examine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ru.paulerokhin.examine.cli.OptionsCli;
import ru.paulerokhin.examine.fileprocessor.strategy.FilePrepareStrategy;
import ru.paulerokhin.examine.fileprocessor.strategy.FilePrepareStrategyFactory;
import ru.paulerokhin.examine.fileprocessor.strategy.FilePrepareStrategyName;
import ru.paulerokhin.examine.wordprocessor.WordSeparator;

import java.io.StringReader;


/**
 * Unit test for WordSeparator.
 */
public class WordSeparatorTest {
    @Mock
    OptionsCli options;

    @Mock
    FilePrepareStrategyFactory strategyFactory;

    @Mock
    FilePrepareStrategy strategy;

    private StringReader text = new StringReader("ALPHA BETA;GAMMA. DELTA\"EPSILON\"");

    @Before
    public void setUp() throws java.io.IOException {
        MockitoAnnotations.initMocks(this);
        Mockito.when(strategyFactory.findStrategy(FilePrepareStrategyName.FilePrepareEcoStrategy)).thenReturn(strategy);
        Mockito.when(strategy.read()).thenAnswer(invocationOnMock -> text.read());
        Mockito.when(options.isSmartHtmlAnalyse()).thenReturn(false);
        Mockito.when(options.isPlainText()).thenReturn(false);
    }

    @Test
    public void getWord() throws java.io.IOException {
        WordSeparator wordSeparator = new WordSeparator(options, strategyFactory);

        assertEquals("ALPHA", wordSeparator.getWord());
        assertEquals("BETA", wordSeparator.getWord());
        assertEquals("GAMMA", wordSeparator.getWord());
        assertEquals("DELTA", wordSeparator.getWord());
        assertEquals("EPSILON", wordSeparator.getWord());
        assertNull(wordSeparator.getWord());
    }
}

