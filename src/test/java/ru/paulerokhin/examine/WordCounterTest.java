package ru.paulerokhin.examine;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ru.paulerokhin.examine.wordprocessor.WordCounter;
import ru.paulerokhin.examine.wordprocessor.WordSeparator;

import java.util.Map;

import static org.junit.Assert.*;

public class WordCounterTest {
    @Mock
    WordSeparator wordSeparator;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.when(wordSeparator.getWord())
                .thenReturn("ALPHA")
                .thenReturn("BETA")
                .thenReturn("ALPHA")
                .thenReturn("ALPHA")
                .thenReturn("GAMMA")
                .thenReturn(null);
    }

    @Test
    public void getStatistic() throws java.io.IOException {
        WordCounter wordCounter = new WordCounter(wordSeparator);

        Map<String, Integer> words = wordCounter.getStatistic();
        int count = words.get("ALPHA");
        assertEquals(3, count);
        count = words.get("BETA");
        assertEquals(1, count);
        count = words.get("GAMMA");
        assertEquals(1, count);
    }
}