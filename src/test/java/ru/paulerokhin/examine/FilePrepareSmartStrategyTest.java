package ru.paulerokhin.examine;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ru.paulerokhin.examine.fileprocessor.strategy.FilePrepareEcoStrategy;
import ru.paulerokhin.examine.pageprocessor.PageDownloader;

import java.io.StringReader;

import static org.junit.Assert.*;

public class FilePrepareSmartStrategyTest {
    @Mock
    PageDownloader pageDownloader;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private StringReader text = new StringReader("GOOD<script asdf=\"asdf\">BAD</script>GOOD<a>GOOD</a><br />GOOD");

    @Test
    public void read() throws java.io.IOException {
        Mockito.when(pageDownloader.downloadFileGetFileReader()).thenReturn(text);

        FilePrepareEcoStrategy strategy = new FilePrepareEcoStrategy(pageDownloader);

        StringBuilder resultText = new StringBuilder();
        int c;
        while ( (c = strategy.read()) != -1 ) {
            resultText.append((char)c);
        }

        assertEquals("GOOD GOOD GOOD  GOOD", resultText.toString());
    }
}