package ru.paulerokhin.examine;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.paulerokhin.examine.cli.OptionsCli;
import ru.paulerokhin.examine.cli.Reporter;

/**
 * WordStat client class
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
            WordStatisticsConfiguration.class
        );
        OptionsCli options = context.getBean("optionsCli", OptionsCli.class);
        options.setArgs(args);
        Reporter reporter = context.getBean("reporter", Reporter.class);
        reporter.print();
    }
}
