package ru.paulerokhin.examine.pageprocessor;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.paulerokhin.examine.cli.OptionsCli;
import ru.paulerokhin.examine.fileprocessor.FileNameGenerator;

/**
 * downloads web page and provide reader interface
 */
@Component
@Lazy
public class PageDownloader {
    private static Logger log = Logger.getLogger("PageDownloader");
    private OptionsCli options;
    private String path;

    public Reader downloadFileGetFileReader() {
        try {
            URL url = new URL(options.getUrl());
            File initialFile = new File(path);
            FileUtils.copyURLToFile(url, initialFile);
            return new FileReader(path);
        } catch (Exception e) {
            log.error("Download error: " + e.getMessage());
            Runtime.getRuntime().exit(1892);
            return null;
        }
    }

    public PageDownloader(OptionsCli options, FileNameGenerator fileNameGenerator) {
        this.options = options;
        this.path = options.getWorkDir() + fileNameGenerator.getFileName();
    }
}
