package ru.paulerokhin.examine.cli;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.paulerokhin.examine.wordprocessor.WordCounter;

import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

/**
 * gets word statistics form counter and print them to stdout
 */
@Component
@Lazy
public class Reporter {
    private static Logger log = Logger.getLogger("Reporter");
    private WordCounter wordCounter;
    private OptionsCli options;

    public void print() {
        try {
            PrintStream ps = new PrintStream(System.out, true, options.getEncoding());
            for (String word : wordCounter.getStatistic().keySet()) {
                String str = new String(word.getBytes(), StandardCharsets.UTF_8);
                ps.println(str + " " + wordCounter.getStatistic().get(word).toString());

            }
        }
        catch (java.io.IOException e) {
            log.error("IO error: " + e.getMessage());
            Runtime.getRuntime().exit(3450);
        }
    }

    public Reporter(OptionsCli optionsCli, WordCounter wordCounter) {
        this.options = optionsCli;
        this.wordCounter = wordCounter;

    }
}
