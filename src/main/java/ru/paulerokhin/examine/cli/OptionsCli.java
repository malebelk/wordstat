package ru.paulerokhin.examine.cli;

import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Reads GNU style CLI arguments
 */
@Component
public class OptionsCli {
    private static Logger log = Logger.getLogger("Options");

    private String encoding;
    private String url;
    public String getUrl() {
        return url;
    }
    private String workDir;
    public String getWorkDir() {
        return workDir;
    }
    private boolean smartHtmlAnalyse = false;
    private boolean plainText = false;

    public boolean isSmartHtmlAnalyse() {
        return smartHtmlAnalyse;
    }
    public boolean isPlainText() {
        return plainText;
    }
    String getEncoding() {
        return encoding;
    }

    public void setArgs(String[] args) {
        Options options = new Options();
        options.addOption(new Option("s", "smart", false,
                "Removes HTML tags using JSOAP. Memory ineffective."));
        options.addOption(new Option("p", "plain", false,
                "Not remove anything. Memory effective."));
        options.addOption(new Option("h", "help", false,
                "Show this message."));
        options.addOption(new Option("w", "work-dir", true,
                "Set work directory."));
        options.addOption(new Option("l", "log", true,
                "Save log to file. Not implemented"));
        options.addOption(new Option("e", "encoding", true,
                "Terminal encoding. CP866 for Windows cmd or ps, UTF-8 for many others, skip for autoselect."));


        CommandLineParser parser = new DefaultParser();
        CommandLine line;
        try {
            line = parser.parse( options, args );
            if (line.hasOption("h")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "java -jar wordstat.jar [options] url", options );
                Runtime.getRuntime().exit(0);
            }

            if (line.hasOption("s")) {
                smartHtmlAnalyse = true;
            } else if (line.hasOption("p")) {
                plainText = true;
            }

            if (line.hasOption("work-dir")) {
                workDir = line.getOptionValue("work-dir");

            } else {
                workDir = System.getProperty("user.dir");
            }
            log.info("Workdir set to " + workDir);

            if (line.hasOption("encoding") && !line.getOptionValue("encoding").equals("")) {
                encoding = line.getOptionValue("encoding");
            } else if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                encoding = "CP866";
            } else {
                encoding = "UTF-8";
            }
            log.info("Encoding set to " + encoding);

            if (!line.getArgList().isEmpty()) {
                url = line.getArgList().get(0);
            } else {
                log.error( "Url not specified. Run java -jar wordstat.jar --help to get help. ");
                Runtime.getRuntime().exit(13);
            }



        }
        catch( ParseException exp ) {
            log.error( "Parsing failed.  Reason: " + exp.getMessage() );
            Runtime.getRuntime().exit(120);
        }
    }

    public OptionsCli() {

    }
}
