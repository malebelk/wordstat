package ru.paulerokhin.examine;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.paulerokhin.examine")
public class WordStatisticsConfiguration {

}
