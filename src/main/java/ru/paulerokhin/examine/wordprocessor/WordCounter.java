package ru.paulerokhin.examine.wordprocessor;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


/**
 * gets words from separator and count them
 * */
@Component
@Lazy
public class WordCounter {
    private static Logger log = Logger.getLogger("WordCounter");
    private WordSeparator wordSeparator;
    private Map<String, Integer> statistic = new HashMap<>();

    public Map<String, Integer> getStatistic() throws java.io.IOException {
        String word;
        while ( (word = wordSeparator.getWord()) != null) {
            this.add(word.replaceAll("&nbsp", ""));
        }

        return statistic;
    }

    private void add(String word) {
        int count = statistic.getOrDefault(word, 0);
        statistic.put(word, count + 1);
    }

    public WordCounter(WordSeparator wordSeparator) {
        this.wordSeparator = wordSeparator;
    }
}
