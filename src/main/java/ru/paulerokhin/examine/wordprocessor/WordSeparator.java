package ru.paulerokhin.examine.wordprocessor;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.paulerokhin.examine.cli.OptionsCli;
import ru.paulerokhin.examine.fileprocessor.strategy.FilePrepareStrategy;
import ru.paulerokhin.examine.fileprocessor.strategy.FilePrepareStrategyFactory;
import ru.paulerokhin.examine.fileprocessor.strategy.FilePrepareStrategyName;

import java.io.IOException;
import java.util.ArrayList;

/**
 * separates words using delimiters
 */
@Component
@Lazy
public class WordSeparator {
    private FilePrepareStrategy input;
    private ArrayList<Character> separators = new ArrayList<>();

    private boolean isSeparator(int i) {
        return separators.contains((char)i);
    }

    public String getWord() throws IOException {
        StringBuilder word = new StringBuilder();
        int buffer;
        while ( (buffer = input.read()) != -1 ) {
            if (isSeparator((char)buffer) ) {
                if ( word.length() > 0 ) {
                    return word.toString();
                }
            } else {
                word.append((char)buffer);
            }
        }
        if (word.length() != 0) {
            return word.toString();
        } else {
            return null;
        }
    }

    public WordSeparator(OptionsCli options, FilePrepareStrategyFactory factory) {
        if (options.isSmartHtmlAnalyse()) {
            input = factory.findStrategy(FilePrepareStrategyName.FilePrepareSmartStrategy);
        } else if (options.isPlainText()) {
            input = factory.findStrategy(FilePrepareStrategyName.FilePreparePlainStrategy);
        } else {
            input = factory.findStrategy(FilePrepareStrategyName.FilePrepareEcoStrategy);
        }
        for (Character separator: new char[]{' ', ',', '.', '!', '?', '"', ';', ':', '[', ']', '(', ')', '\n', '\r', '\t'}
        ) {
            separators.add(separator);
        }
    }
}
