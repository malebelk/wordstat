package ru.paulerokhin.examine.fileprocessor;


import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.paulerokhin.examine.cli.OptionsCli;

@Component
@Lazy
public class FileNameGenerator {
    private String filename;

    public String getFileName() {
        return filename;
    }

    public FileNameGenerator(OptionsCli options) {
        filename = options.getUrl().replace(':', '-')
                .replace('/', '-')
                .replace('?', '-') + ".txt";
    }
}
