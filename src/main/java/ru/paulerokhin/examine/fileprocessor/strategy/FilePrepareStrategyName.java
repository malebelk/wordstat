package ru.paulerokhin.examine.fileprocessor.strategy;

public enum FilePrepareStrategyName {
    FilePrepareEcoStrategy,
    FilePrepareSmartStrategy,
    FilePreparePlainStrategy
}
