package ru.paulerokhin.examine.fileprocessor.strategy;

import javafx.util.Pair;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.paulerokhin.examine.pageprocessor.PageDownloader;

import java.io.Reader;
import java.util.ArrayList;

/**
 * prepares file for processing by removing html tags
 */
@Component
@Lazy
public class FilePrepareEcoStrategy implements FilePrepareStrategy {
    private Reader input;
    private ArrayList<Pair<String, String>> exclusionsWithInnerHtml = new ArrayList<>();
    private ArrayList<String> exclusions = new ArrayList<>();

    @Override
    public FilePrepareStrategyName getStrategyName() {
        return FilePrepareStrategyName.FilePrepareEcoStrategy;
    }

    private String checkExcludeInnerHtml(String word) {
        for (Pair<String, String> rePair : exclusionsWithInnerHtml) {
            if (word.matches(rePair.getKey())) {
                return rePair.getValue();
            }
        }
        return "";
    }

    private boolean checkExclude(String word) {
        for (String re : exclusions) {
            if (word.matches(re)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int read() throws java.io.IOException {
        int c;
        boolean exclusionSearch = false;
        String exclusionCloseTagRe = "";

        StringBuilder buffer = new StringBuilder();

        while ((c = input.read()) != -1) {
            buffer.append((char) c);
            String word = buffer.toString();
            if (c == '>' && !exclusionCloseTagRe.equals("") && word.matches(exclusionCloseTagRe)) {
                return ' '; // return space to delimit words around deleted tag
            }
            else if (c == '>' && exclusionSearch && exclusionCloseTagRe.equals("")) {
                exclusionCloseTagRe = checkExcludeInnerHtml(word);
                if (exclusionCloseTagRe.equals("") && checkExclude(word)) {
                    return ' '; // return space to delimit words around deleted tag
                }
            }
            else if (c == '<') {
                exclusionSearch = true;
            }
            else if (!exclusionSearch) {
                return c;
            }
        }
        return c;
    }

    public FilePrepareEcoStrategy(PageDownloader pageDownloader) {
        exclusionsWithInnerHtml.add(new Pair<>("\\<head.*?>", "(?s)\\<head.*?>.*?\\<\\/head.*?>"));
        exclusionsWithInnerHtml.add(new Pair<>("\\<script.*?>", "(?s)\\<script.*?>.*?\\<\\/script.*?>"));
        exclusionsWithInnerHtml.add(new Pair<>("\\<style.*?>", "(?s)\\<style.*?>.*?\\<\\/style.*?>"));
        exclusions.add("(?s)\\<.*?>");
        this.input = pageDownloader.downloadFileGetFileReader();
    }
}
