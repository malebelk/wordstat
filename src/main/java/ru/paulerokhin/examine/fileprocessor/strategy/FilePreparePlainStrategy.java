package ru.paulerokhin.examine.fileprocessor.strategy;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.paulerokhin.examine.pageprocessor.PageDownloader;

import java.io.IOException;
import java.io.Reader;


/**
 * Plain text strategy. Just dummy thing.
 */
@Component
@Lazy
public class FilePreparePlainStrategy implements FilePrepareStrategy {
    private Reader input;

    @Override
    public int read() throws IOException {
        return input.read();
    }

    private void setInput(Reader input) {
            this.input = input;
    }

    @Override
    public FilePrepareStrategyName getStrategyName() {
        return FilePrepareStrategyName.FilePreparePlainStrategy;
    }

    public FilePreparePlainStrategy(PageDownloader pageDownloader) {
        setInput(pageDownloader.downloadFileGetFileReader());
    }
}
