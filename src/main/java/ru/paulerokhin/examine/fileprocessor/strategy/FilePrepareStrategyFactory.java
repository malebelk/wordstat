package ru.paulerokhin.examine.fileprocessor.strategy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class FilePrepareStrategyFactory {
    private Map<FilePrepareStrategyName, FilePrepareStrategy> strategies;

    @Autowired
    public FilePrepareStrategyFactory(Set<FilePrepareStrategy> strategySet) {
        createStrategy(strategySet);
    }

    public FilePrepareStrategy findStrategy(FilePrepareStrategyName strategyName) {
        return strategies.get(strategyName);
    }

    private void createStrategy(Set<FilePrepareStrategy> strategySet) {
        strategies = new HashMap<FilePrepareStrategyName, FilePrepareStrategy>();
        strategySet.forEach(
                strategy -> strategies.put(strategy.getStrategyName(), strategy));
    }
}

