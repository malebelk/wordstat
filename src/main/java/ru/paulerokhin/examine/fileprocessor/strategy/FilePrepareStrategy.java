package ru.paulerokhin.examine.fileprocessor.strategy;

public interface FilePrepareStrategy {
    int read() throws java.io.IOException ;
    FilePrepareStrategyName getStrategyName();
}
