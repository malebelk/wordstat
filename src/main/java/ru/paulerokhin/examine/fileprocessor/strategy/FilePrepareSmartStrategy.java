package ru.paulerokhin.examine.fileprocessor.strategy;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.paulerokhin.examine.pageprocessor.PageDownloader;

import java.io.Reader;
import java.io.StringReader;

/**
 * Experimental file prepare strategy uses Jsoup
 */
@Component
@Lazy
public class FilePrepareSmartStrategy implements FilePrepareStrategy {
    private StringReader text;

    private void setInput(Reader input) {
        try {
            int c;
            StringBuilder text = new StringBuilder();
            while ((c = input.read()) != -1) {
                text.append((char)c);
            }
            Document doc = Jsoup.parse(text.toString());
            this.text = new StringReader(doc.text());
        }
        catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public FilePrepareStrategyName getStrategyName() {
        return FilePrepareStrategyName.FilePrepareSmartStrategy;
    }

    @Override
    public int read() throws java.io.IOException {
        return text.read();
    }

    public FilePrepareSmartStrategy(PageDownloader pageDownloader) {
        setInput(pageDownloader.downloadFileGetFileReader());
    }
}
